<?php

namespace Drupal\webform_gigya\Plugin\WebformHandler;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gigya\CmsStarterKit\sdk\GSApiException;
use Drupal\gigya\CmsStarterKit\sdk\GSObject;
use Drupal\gigya\CmsStarterKit\sdk\GSResponse;
use Drupal\gigya\Helper\GigyaHelper;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformMessageManagerInterface;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Webform submission lite account registration handler.
 *
 * @WebformHandler(
 *   id = "lite_account_registration",
 *   label = @Translation("Gigya Lite Account Registration"),
 *   category = @Translation("External"),
 *   description = @Translation("Call to Gigya API to register lite account."),
 *   tokens = TRUE,
 * )
 */
class LiteAccountRegistrationHandler extends WebformHandlerBase {

  const RESPONSE_DATA_DEFAULT = [
    'callId' => '',
    'errorCode' => '',
    'apiVersion' => '',
    'statusCode' => '',
    'statusReason' => '',
    'time' => '',
    'UID' => '',
  ];

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $tokenManager;

  /**
   * The webform message manager.
   *
   * @var \Drupal\webform\WebformMessageManagerInterface
   */
  protected $messageManager;

  /**
   * A webform element plugin manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $elementManager;

  /**
   * List of unsupported webform submission properties.
   *
   * The below properties will not being included in a remote post.
   *
   * @var array
   */
  protected $unsupportedProperties = [
    'metatag',
  ];

  /**
   * Drupal\gigya\Helper\GigyaHelper.
   *
   * @var \Drupal\gigya\Helper\GigyaHelper
   */
  protected $gigyaHelper;

  /**
   * Drupal\Core\Logger\LoggerChannelInterface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->httpClient = $container->get('http_client');
    $instance->tokenManager = $container->get('webform.token_manager');
    $instance->messageManager = $container->get('webform.message_manager');
    $instance->elementManager = $container->get('plugin.manager.webform.element');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $instance->requestStack = $container->get('request_stack');
    $instance->kernel = $container->get('kernel');


    if (class_exists('Drupal\gigya\Helper\GigyaHelper')) {
      $instance->gigyaHelper = new GigyaHelper();
    }
    else {
      $instance->messenger->addError('Gigya module is missing.');
    }
    $instance->logger = $instance->getLogger('webform_gigya');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'];
    return [
        '#settings' => $settings,
      ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'custom_data' => '',
      'debug' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    // Additional.
    $form['additional'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Additional settings'),
    ];
    $form['additional']['custom_data'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Custom parameters'),
      '#description' => $this->t('Enter custom parameters that will be included in all remote post requests.'),
      '#suffix' => 'Example: <pre>data: 
 phone: "[webform_submission:values:phone]"
profile:
 email: "[webform_submission:values:email]"
 gender: "[webform_submission:values:gender]"
 firstName: "[webform_submission:values:name]"
 age: "[webform_submission:values:age]"</pre>',
      '#default_value' => $this->configuration['custom_data'],
    ];

    // Development.
    $form['development'] = [
      '#type' => 'details',
      '#title' => $this->t('Development settings'),
    ];
    $form['development']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('If checked, the data will be dump to screen.'),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['debug'],
    ];

    $form['handler_token'] = [
      '#type' => 'details',
      '#title' => $this->t('Handler response token'),
    ];
    $token = [];
    foreach (self::RESPONSE_DATA_DEFAULT as $key => $item) {
      $token[] = '[webform:handler:' . $this->getHandlerId() . ':' . $key . ']';
    }
    $form['handler_token']['title'] = [
      '#markup' => '<h3>' . $this->t('Use below token if you want to store response data:') . '</h3>',
    ];
    $form['handler_token']['token'] = [
      '#title' => $this->t('Use below token if you want to store response data:'),
      '#markup' => implode('<br/>', $token),
    ];

    $this->elementTokenValidate($form);

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $this->sendRequest($webform_submission);
  }

  /**
   * {@inheritdoc}
   */
  public function postDelete(WebformSubmissionInterface $webform_submission) {
  }

  /**
   * Send request to Gigya.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Drupal\webform\WebformSubmissionInterface.
   *
   * @throws \Exception
   */
  protected function sendRequest(WebformSubmissionInterface $webform_submission) {
    $data = $webform_submission->getData();
    $params = new GSObject();
    $params->put('isLite', TRUE);
    $this->debug($params, 'CALL accounts.initRegistration');
    $response = $this->gigyaHelper->sendApiCall('accounts.initRegistration', $params);
    $this->debug($response, 'CALL accounts.initRegistration');
    if ($response instanceof GSApiException) {
      // Clear token.
      $this->saveResultAfterRequest($webform_submission, []);
      return;
    }
    $regToken = $response->getString('regToken');
    if (empty($regToken)) {
      $this->logger->error('Gigya regToken is missing');
    }

    // Get request options with tokens replaced.
    $request_options = (!empty($this->configuration['custom_data'])) ? Yaml::decode($this->configuration['custom_data']) : [];
    $request_options = $this->replaceTokens($request_options, $webform_submission);
    if (empty($request_options['regToken'])) {
      $request_options['regToken'] = $regToken;
    }
    if (empty($request_options['profile'])) {
      $request_options['profile'] = $data;
    }
    $params = new GSObject(Json::encode($request_options));
    $this->debug($params, 'CALL accounts.setAccountInfo');
    $response = $this->gigyaHelper->sendApiCall('accounts.setAccountInfo', $params);
    $this->debug($response, 'CALL accounts.setAccountInfo');
    if ($response instanceof GSApiException) {
      // Clear token.
      $this->saveResultAfterRequest($webform_submission, []);
      return;
    }
    $gsObject = $response->getData();

    if (!empty($gsObject)) {
      $gigya_data = $gsObject->serialize();
    }
    $this->saveResultAfterRequest($webform_submission, $gigya_data);
  }

  /**
   * Save result of Gigya response.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   Drupal\webform\WebformSubmissionInterface.
   * @param array $gigya_data
   *   Gigya response.
   */
  protected function saveResultAfterRequest(WebformSubmissionInterface $webform_submission, array $gigya_data = []) {
    if (empty($gigya_data)) {
      $gigya_data = self::RESPONSE_DATA_DEFAULT;
    }
    // Replace [webform:handler] tokens in submission data.
    // Data structured for [webform:handler:remote_post:completed:key] tokens.
    $submission_data = $webform_submission->getData();
    $submission_has_token = (strpos(print_r($submission_data, TRUE), '[webform:handler:' . $this->getHandlerId() . ':') !== FALSE) ? TRUE : FALSE;
    if ($submission_has_token) {
      $token_data = ['webform_handler' => [$this->getHandlerId() => $gigya_data]];
      $submission_data = $this->replaceTokens($submission_data, $webform_submission, $token_data, ['clear' => TRUE]);
      $webform_submission->setData($submission_data);
      if ($this->isResultsEnabled()) {
        $webform_submission->resave();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function buildTokenTreeElement(array $token_types = [
    'webform',
    'webform_submission',
  ], $description = NULL) {
    $description = $description ?: $this->t('Use [webform_submission:values:ELEMENT_KEY:raw] to get plain text values.');
    return parent::buildTokenTreeElement($token_types, $description);
  }

  /**
   * Determine if saving of results is enabled.
   *
   * @return bool
   *   TRUE if saving of results is enabled.
   */
  protected function isResultsEnabled() {
    return ($this->getWebform()->getSetting('results_disabled') === FALSE);
  }

  /**
   * Debug message.
   *
   * @param \Drupal\gigya\CmsStarterKit\sdk\GSResponse $response
   *   Drupal\gigya\CmsStarterKit\sdk\GSResponse.
   * @param string $title
   *   Title.
   */
  private function debug($response, $title = '') {
    if (empty($this->configuration['debug'])) {
      return;
    }
    dump($response);
  }

}

